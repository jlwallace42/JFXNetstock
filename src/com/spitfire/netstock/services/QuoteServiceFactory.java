package com.spitfire.netstock.services;

public class QuoteServiceFactory
{
    public static IQuoteService getQuoteService(QuoteServiceType type)
    {
        IQuoteService result = null;
        if ( type == QuoteServiceType.YahooFinanceAPI )
        {
            result = new YahooQuoteService();
        }
        else if ( type == QuoteServiceType.ETradeAPI )
        {
            result = new ETradeApiQuoteService();
        }
        
        return result;
    }
}
