package com.spitfire.netstock.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.stock.StockDividend;
import yahoofinance.quotes.stock.StockQuote;
import yahoofinance.quotes.stock.StockStats;

import com.spitfire.netstock.model.Quote;

/**
 * Quote retrieval service for Yahoo Finance.
 */
public class YahooQuoteService implements IQuoteService
{
    
    public YahooQuoteService()
    {
        // Not currently needed.
    }

    @Override
    public void init(Map<String, Object> configuration)
    {
        // Not currently needed.
    }

    @Override
    public Object connect()
    {
        // Not currently needed.
        return null;
    }

    @Override
    public List<Quote> fetchQuotes(Object handle, String... tickers) throws IOException
    {
        List<Quote> quotes = new ArrayList<Quote>(tickers.length);
        Map<String, Stock> yahooStocks = YahooFinance.get(tickers);
        for (String ticker : tickers)
        {
            Stock yahooStock = yahooStocks.get(ticker);
            if ( yahooStock != null )
            {
                Quote quote = cvtToQuote(yahooStock);
                if ( quote != null )
                    quotes.add(quote);
            }
        }
        
        return quotes;
    }

    @Override
    public void disconnet(Object handle)
    {
        // Not currently needed.
    }
    
    
    private Quote cvtToQuote(Stock yahooStock)
    {
        StockQuote yahooQuote   = yahooStock.getQuote();        
        String symbol           = yahooStock.getSymbol();
        String name             = yahooStock.getName();
        BigDecimal price        = yahooQuote.getPrice();
        BigDecimal change       = yahooQuote.getChange();
        BigDecimal changeInPct  = yahooQuote.getChangeInPercent();
        Calendar lastTradeTime  = yahooQuote.getLastTradeTime();
        long volume             = yahooQuote.getVolume();
        BigDecimal yearHigh     = yahooQuote.getYearHigh();
        BigDecimal yearLow      = yahooQuote.getYearLow();
        BigDecimal open         = yahooQuote.getOpen();
        BigDecimal prevClose    = yahooQuote.getPreviousClose();
        
        StockStats yahooStats   = yahooStock.getStats();
        BigDecimal pe           = yahooStats.getPe();
        
        StockDividend yahooDividend = yahooStock.getDividend();
        Calendar divExDate      = yahooDividend.getExDate();
        BigDecimal annualYield  = yahooDividend.getAnnualYield();
        
        
        Quote quote = new Quote(symbol);
        quote.setPrice(     (price != null)       ? price.doubleValue()       : 0.0f);
        quote.setChange(    (change != null)      ? change.doubleValue()      : 0.0f);
        quote.setChangePct( (changeInPct != null) ? changeInPct.doubleValue() : 0.0f);        
        quote.setQuoteDate( calendarToLocalDate(lastTradeTime) );
        quote.setQuoteTime( calendarToLocalTime(lastTradeTime) );
        quote.setVolume((int)volume);
        quote.setHigh52(    (yearHigh != null)    ? yearHigh.doubleValue()    : 0.0f);
        quote.setLow52(     (yearLow  != null)    ? yearLow.doubleValue()     : 0.0f);
        quote.setPeRatio(   (pe != null)          ? pe.doubleValue()          : 0.0f);
        quote.setExDivDate( calendarToLocalDate(divExDate) );
        quote.setDividend(  (annualYield != null) ? annualYield.doubleValue() : 0.0f);
        quote.setFullName(name);
        quote.setOpen(      (open != null)        ? open.doubleValue()        : 0.0f);
        quote.setPrevClose( (prevClose != null)   ? prevClose.doubleValue()   : 0.0f);
         
        return quote;
    }
    
    private LocalDate calendarToLocalDate(Calendar cal)
    {
        LocalDate result = null;
        if ( cal != null )
        {
            int year        = cal.get(Calendar.YEAR);
            int month       = cal.get(Calendar.MONTH);
            int dayOfMonth  = cal.get(Calendar.DAY_OF_MONTH);
            
            result =  LocalDate.of(year, month+1, dayOfMonth);
        }
        
        return result;
    }
    
    private LocalTime calendarToLocalTime(Calendar cal)
    {
        LocalTime result = null;
        if ( cal != null)
        {
            int hour        = cal.get(Calendar.HOUR_OF_DAY);
            int minutes     = cal.get(Calendar.MINUTE);
            int seconds     = cal.get(Calendar.SECOND);
            
            result = LocalTime.of(hour, minutes, seconds);
        }
        
        return result;
    }
}
