package com.spitfire.netstock.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.etrade.etws.market.AllQuote;
import com.etrade.etws.market.DetailFlag;
import com.etrade.etws.market.DetailProduct;
import com.etrade.etws.market.QuoteData;

import com.spitfire.etrade.connection.ETradeAccessMgr;
import com.spitfire.etrade.connection.IETradeAuthenticator;
import com.spitfire.etrade.connection.JFxWebViewAuthenticator;
import com.spitfire.etrade.services.ETradeMarketServices;
import com.spitfire.netstock.model.Quote;

/**
 * Retrieval quote service based upon ETrade API.
 * 
 * @author jlwallace
 */
public class ETradeApiQuoteService implements IQuoteService
{
    private static DateTimeFormatter DTFormat       = DateTimeFormatter.ofPattern("HH:mm:ss zzz MM-dd-yyyy");
    private static DateTimeFormatter ExDivFormat    = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    
    @Override
    public void init(Map<String,Object> configuration)
    {
        // Not currently needed.
    }

    @Override
    public Object connect()
    {
        // Not currently needed.
        return null;
    }

    @Override
    public List<Quote> fetchQuotes(Object handle, String... tickers) throws Exception
    {
        IETradeAuthenticator authenticator        = new JFxWebViewAuthenticator();
        ETradeAccessMgr etradeAccessMgr           = new ETradeAccessMgr(authenticator);
        ETradeMarketServices etradeMarketServices = new ETradeMarketServices(etradeAccessMgr);
        
        List<String> tickerList = toArrayList(tickers);
        List<QuoteData> etradeQuoteDataList = null;
        try {
            etradeQuoteDataList = etradeMarketServices.getQuotes(tickerList, DetailFlag.ALL);
        }
        catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
        
        List<Quote> resultQuotes = new ArrayList<Quote>(etradeQuoteDataList.size());
        for (QuoteData etradeQuoteData : etradeQuoteDataList)
        {
            String dateTimeStr          = etradeQuoteData.getDateTime();
            DetailProduct detailProduct = etradeQuoteData.getProduct();
            AllQuote allQuote           = etradeQuoteData.getAll();
            
            Quote newQuote = cvtToQuote(dateTimeStr, detailProduct, allQuote);
            resultQuotes.add(newQuote);
        }
        
        Collections.sort(resultQuotes);
        return resultQuotes;
    }

    @Override
    public void disconnet(Object handle)
    {
        // Not currently needed.
    }
    
    private Quote cvtToQuote(String dateTimeStr, DetailProduct detailProduct, AllQuote allQuote)
    {
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, DTFormat);
        String exDivDateStr = allQuote.getExDivDate();
        LocalDate exDivDate = null;
        if (exDivDateStr != null && !exDivDateStr.isEmpty() ) {
            exDivDate = LocalDate.parse(exDivDateStr, ExDivFormat);
        }
        
        Quote q = new Quote(detailProduct.getSymbol());
        q.setPrice(allQuote.getLastTrade());
        q.setChange(allQuote.getChgClose());
        q.setChangePct(allQuote.getChgClosePrcn());
        q.setQuoteDate(dateTime.toLocalDate());
        q.setQuoteTime(dateTime.toLocalTime());
        q.setVolume(Long.valueOf(allQuote.getTotalVolume()).intValue());
        q.setHigh52(allQuote.getHigh52());
        q.setLow52(allQuote.getLow52());
        q.setExDivDate(exDivDate);
        q.setDividend(allQuote.getDividend());
        q.setFullName(allQuote.getSymbolDesc());
        q.setOpen(allQuote.getOpen());
        q.setPrevClose(allQuote.getPrevClose());
        
        return q;
    }

    
    /**
     * Used instead of Arrays.toList because ETradeAPI does a hard cast
     * to java.util.ArrayList.
     * 
     * @param strings   an Array of String to convert.
     * 
     * @return a new ArrayList<String> instance.
     */
    private ArrayList<String> toArrayList(String... strings)
    {
        ArrayList<String> arrayList = new ArrayList<String>(strings.length);
        for (String s : strings) {
            arrayList.add(s);
        }
        
        return arrayList;
    }
}
