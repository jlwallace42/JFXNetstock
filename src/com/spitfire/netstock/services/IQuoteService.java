package com.spitfire.netstock.services;

import java.util.List;
import java.util.Map;

import com.spitfire.netstock.model.Quote;

/**
 * Interface that defines quote retrievals.
 */
public interface IQuoteService
{
    /**
     * Initializes the quote service.
     * 
     * @param configuration Map of key/value pairs applicable
     *                      to the specific service.
     */
    public void init(Map<String, Object> configuration);
    
    /**
     * Performs any necessary connection activities
     * (e.g., login, define session, etc.).
     * 
     * @return An opaque object representing a handle to be used
     *         to represent this connection.
     */
    public Object connect();
    
    /**
     * Fetch quotes for the specified ticker symbols using the
     * quote service connection defined by the opaque handle.
     * 
     * @param handle    the opaque connection handle returned by
     *                  a previous call to connect().
     * @param tickers   one or more ticker symbols representing
     *                  the securities to fetch quotes for.
     *                  
     * @return  A collection of Quote instances.
     */
    public List<Quote> fetchQuotes(Object handle, String... tickers) throws Exception;
    
    /**
     * Disconnect and release any resources associated with a previously
     * opened connection.
     * 
     * @param handle    the opaque connection handler returned by the
     *                  original call to connect().
     */
    public void disconnet(Object handle);
}
