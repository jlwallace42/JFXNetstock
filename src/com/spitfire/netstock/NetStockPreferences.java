package com.spitfire.netstock;

import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.spitfire.netstock.model.ColumnLayout;

@XmlRootElement(name = "preferences")
public class NetStockPreferences
{
    private SortedSet<String>   tickerSymbolList;
    private String              quoteSourceName;
    private Boolean             autoExportEnabled;
    private String              autoExportFilePath;
    private List<ColumnLayout>  displayColumnLayoutList;
    private List<ColumnLayout>  exportColumnLayoutList;
    private Boolean             autoUpdateEnabled;
    private Integer             autoUpdateIntervalInSec;
    private Integer             networkTimeoutInSec;
    
    public NetStockPreferences()
    {
        this.tickerSymbolList   = new TreeSet<String>(Arrays.asList("ALK", "BRK-B", "CAT", "CHTTX", "TSCO"));
        this.quoteSourceName    = "YahooFinanceAPI";
        this.autoExportEnabled  = Boolean.TRUE;
        this.autoExportFilePath = "C:/apps/NetStock/autoexp.txt";
        this.displayColumnLayoutList = ColumnLayout.getDefaultColumnLayoutList();
        this.exportColumnLayoutList  = ColumnLayout.getDefaultColumnLayoutList();
        this.autoUpdateEnabled  = Boolean.FALSE;
        this.autoUpdateIntervalInSec = 60;
        this.networkTimeoutInSec= 10;
    }
    
    
    @XmlElementWrapper(name = "tickerSymbols")
    @XmlElement(name = "tickerSymbol")
    public SortedSet<String> getTickerSymbols()
    {
        return tickerSymbolList;
    }
    
    public void setTickerSymbols(SortedSet<String> tickerSymbolsList)
    {
        this.tickerSymbolList = tickerSymbolsList;
    }
    
    
    @XmlElement(name = "quoteSourceName")
    public String getQuoteSourceName()
    {
        return quoteSourceName;
    }
    
    public void setQuoteSourceName(String quoteSourceName)
    {
        this.quoteSourceName = quoteSourceName;
    }
    
 
    @XmlElement(name = "autoExportEnabled")
    public Boolean getAutoExportEnabled()
    {
        return autoExportEnabled;
    }
    
    public void setAutoExportEnabled(Boolean autoExportEnabled)
    {
        this.autoExportEnabled = autoExportEnabled;
    }
    
    
    @XmlElement(name = "autoExportFilePath")
    public String getAutoExportFilePath()
    {
        return autoExportFilePath;
    }
    
    public void setAutoExportFilePath(String autoExportFilePath)
    {
        this.autoExportFilePath = autoExportFilePath;
    }
    
    
    @XmlElementWrapper(name = "displayColumnLayouts")
    @XmlElement(name = "columnLayout")
    public List<ColumnLayout> getDisplayColumnLayoutList()
    {
        return displayColumnLayoutList;
    }
    
    public void setDisplayColumnLayoutList(List<ColumnLayout> columnLayoutList)
    {
        this.displayColumnLayoutList = columnLayoutList;
    }
    
    
    @XmlElementWrapper(name = "exportColumnLayouts")
    @XmlElement(name = "columnLayout")
    public List<ColumnLayout> getExportColumnLayoutList()
    {
        return exportColumnLayoutList;
    }
    
    public void setExportColumnLayoutList(List<ColumnLayout> columnLayoutList)
    {
        this.exportColumnLayoutList = columnLayoutList;
    }
    
    
    @XmlElement(name = "autoUpdateEnabled")
    public Boolean getAutoUpdateEnabled()
    {
        return autoUpdateEnabled;
    }
    
    public void setAutoUpdateEnabled(Boolean autoUpdateEnabled)
    {
        this.autoUpdateEnabled = autoUpdateEnabled;
    }
    
    
    @XmlElement(name = "autoUpdateInterval")
    public Integer getAutoUpdateIntervalInSec()
    {
        return autoUpdateIntervalInSec;
    }
    
    public void setAutoUpdateIntervalInSec(Integer autoUpdateIntervalInSec)
    {
        this.autoUpdateIntervalInSec = autoUpdateIntervalInSec;
    }
    
    
    @XmlElement(name = "networkTimeout")
    public Integer getNetworkTimeoutInSec()
    {
        return networkTimeoutInSec;
    }
    
    public void setNetworkTimeoutInSec(Integer networkTimeoutInSec)
    {
        this.networkTimeoutInSec = networkTimeoutInSec;
    }
    
}
