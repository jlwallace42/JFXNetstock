package com.spitfire.netstock;

import java.util.List;
import java.util.SortedSet;

import com.spitfire.netstock.model.Quote;
import com.spitfire.netstock.services.IQuoteService;
import com.spitfire.netstock.services.QuoteServiceFactory;
import com.spitfire.netstock.services.QuoteServiceType;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * Asynchronous service class used to fetch
 * security quotes for updating the UI.
 */
public class QuoteFetchWorker extends Service<List<Quote>>
{
    private NetStockPreferences prefs;
    
    public QuoteFetchWorker(NetStockPreferences prefs)
    {
        this.prefs = prefs;
    }
    
    
    @Override
    protected Task<List<Quote>> createTask()
    {
        return new Task<List<Quote>>() {
            @Override
            protected List<Quote> call() throws Exception
            {
                SortedSet<String> tickerSymbols = prefs.getTickerSymbols();
                String[] tickers = tickerSymbols.toArray(new String[tickerSymbols.size()]);
                
                String quoteServiceName    = prefs.getQuoteSourceName();
                QuoteServiceType quoteServiceType = QuoteServiceType.valueOf(quoteServiceName);
                IQuoteService quoteService = QuoteServiceFactory.getQuoteService(quoteServiceType);
                quoteService.init(null);
                Object handle = quoteService.connect();
                List<Quote> quotes = quoteService.fetchQuotes(handle, tickers);
                quoteService.disconnet(handle);
                
                return quotes;
            }           
        };
    }
        
}
