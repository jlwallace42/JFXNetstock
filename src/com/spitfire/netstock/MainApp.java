package com.spitfire.netstock;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.spitfire.netstock.QuoteFetchWorker;
import com.spitfire.netstock.model.ColumnLayout;
import com.spitfire.netstock.model.Quote;
import com.spitfire.netstock.view.AddSecurityDialogController;
import com.spitfire.netstock.view.DisplaySettingsDialogController;
import com.spitfire.netstock.view.ExportSettingsDialogController;
import com.spitfire.netstock.view.RootLayoutController;
import com.spitfire.netstock.view.SecurityTableViewController;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MainApp extends Application {
    public static final String  DefaultApplPrefsFilePath    = "./NetStockPrefs.xml";
    
    private static final DateTimeFormatter TimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter DateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private static final DecimalFormat     DecFormatter  = new DecimalFormat("#0.00");
    private static final DecimalFormat     VolFormatter  = new DecimalFormat("###,###,###");

    private static String   ApplPrefsFilePath;
    
    private Stage                   primaryStage;
    private BorderPane              rootLayout;
    
    private NetStockPreferences     appPrefs;
    private QuoteFetchWorker        quoteService;
    private ObservableList<Quote>   quoteData = FXCollections.observableArrayList();
    private SecurityTableViewController securityTableViewController;

    public static void main(String[] args) {
        ApplPrefsFilePath = DefaultApplPrefsFilePath;
        if ( args != null && args.length > 0 )
        {
            if ( args.length >= 2 && "-f".equalsIgnoreCase(args[0]) )
            {
                ApplPrefsFilePath = args[1];
            }
        }
        
        launch(args);
    }

    
    public MainApp() {
        appPrefs = loadApplPreferences(ApplPrefsFilePath);
        saveApplPreferences(ApplPrefsFilePath, appPrefs);
        
        SortedSet<String> tickerSymbols = appPrefs.getTickerSymbols();
        for (String tickerSymbol : tickerSymbols) {
            Quote aQuote = new Quote(tickerSymbol);
            quoteData.add( aQuote );
        }
    }

    @Override
    public void start(Stage primaryStage) 
    {
        this.quoteService = new QuoteFetchWorker(appPrefs);
        this.quoteService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t)
            {
                List<Quote> newQuotes = quoteService.getValue();
                quoteData.clear();
                quoteData.addAll(newQuotes);
                
                exportQuotes();
            }
        });
        
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("JFX NetStock - '" + ApplPrefsFilePath + "'");
        
        initRootLayout();       
        showSecurityTableView();
    }
    
    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane)(loader.load());
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            
            RootLayoutController rootLayoutCtlr = loader.getController();
            rootLayoutCtlr.setMainApp(this);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Show the security table view inside the root layout.
     */
    public void showSecurityTableView() {
        try {
            SecurityTableViewController.initColumnLayouts(appPrefs.getDisplayColumnLayoutList());
            
            // Load security table view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/SecurityTableView.fxml"));
            AnchorPane securityTableView = (AnchorPane)(loader.load());
            
            // Set the security table view into the center of the root layout.
            rootLayout.setCenter(securityTableView);
            
            // Give controller access to the main app.
            securityTableViewController = loader.getController();
            securityTableViewController.setMainApp(this);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public NetStockPreferences loadApplPreferences(String prefsFilePath)
    {
        NetStockPreferences prefs = null;
        try {
            File prefsFile = new File(prefsFilePath).getCanonicalFile();
            
            JAXBContext jaxbCtx = JAXBContext.newInstance(NetStockPreferences.class);
            Unmarshaller um = jaxbCtx.createUnmarshaller();
            prefs = (NetStockPreferences)(um.unmarshal(prefsFile));
        }
        catch (Exception e) {
            e.printStackTrace();
            prefs = new NetStockPreferences();
        }
                
        return prefs;
    }
    
    public void saveApplPreferences(String prefsFilePath, NetStockPreferences prefs)
    {
        try {
            File prefsFile = new File(prefsFilePath);
            
            JAXBContext jaxbCtx = JAXBContext.newInstance(NetStockPreferences.class);
            Marshaller m = jaxbCtx.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(prefs, prefsFile);
        }
        catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }
    
    public ObservableList<Quote> getQuoteData() {
        return quoteData;
    }

    public void showAddSecurityDialog()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/AddSecurityDialog.fxml"));
            AnchorPane dialogPane = (AnchorPane)(loader.load());
            AddSecurityDialogController dialogController = loader.getController();
            Stage dialogStage = dialogController.setupDialogStage(this, dialogPane, primaryStage);            
            dialogStage.showAndWait();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public boolean showDeleteSecurityDialog(String ticker)
    {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirm Security Deletion");
        alert.setHeaderText(null);
        alert.setContentText("Delete security '" + ticker + "'?");
        Optional<ButtonType> alertResult = alert.showAndWait();
        
        if ( alertResult.get() == ButtonType.OK )
        {
            removeSecuritySymbol(ticker);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public void showDisplaySettingsDialog()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/DisplaySettingsDialog.fxml"));
            Pane dialogPane = (Pane)(loader.load());
            DisplaySettingsDialogController dialogController = loader.getController();
            dialogController.setSelectedList(appPrefs.getDisplayColumnLayoutList());
            
            Stage dialogStage = dialogController.setupDialogStage(this, dialogPane, primaryStage);
            dialogStage.showAndWait();

            if ( dialogController.wasSuccessfulOK() )
            {
                List<ColumnLayout> displayColumnList = dialogController.getSelectedList();
                appPrefs.setDisplayColumnLayoutList(displayColumnList);
                saveApplPreferences(ApplPrefsFilePath, appPrefs);
                
                securityTableViewController.resetColumns(displayColumnList);
            }
            
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void showExportSettingsDialog()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ExportSettingsDialog.fxml"));
            Pane dialogPane = (Pane)(loader.load());
            ExportSettingsDialogController dialogController = loader.getController();
            dialogController.setOutputFilePath(appPrefs.getAutoExportFilePath());
            dialogController.setSelectedList(appPrefs.getExportColumnLayoutList());
            
            Stage dialogStage = dialogController.setupDialogStage(this, dialogPane, primaryStage);
            dialogStage.showAndWait();

            if ( dialogController.wasSuccessfulOK() )
            {
                appPrefs.setAutoExportFilePath(dialogController.getOutputFilePath());
                appPrefs.setExportColumnLayoutList(dialogController.getSelectedList());
                saveApplPreferences(ApplPrefsFilePath, appPrefs);
            }
            
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void addNewSecuritySymbol(String newSymbol)
    {
        SortedSet<String> tickerSymbols = appPrefs.getTickerSymbols();
        tickerSymbols.add(newSymbol);
        saveApplPreferences(ApplPrefsFilePath, appPrefs);
        
        ObservableList<Quote> quotes = getQuoteData();
        for (Quote quote : quotes)
        {
            if ( quote.getTicker().equalsIgnoreCase(newSymbol) )
                return;
        }
        
        quotes.add( new Quote(newSymbol) );
    }
    
    public void removeSecuritySymbol(String symbol)
    {
        SortedSet<String> tickerSymbols = appPrefs.getTickerSymbols();
        tickerSymbols.remove(symbol);
        saveApplPreferences(ApplPrefsFilePath, appPrefs);
        
        ObservableList<Quote> quotes = getQuoteData();
        for (int i = 0; i < quotes.size(); i++)
        {
            if ( quotes.get(i).getTicker().equalsIgnoreCase(symbol) )
            {
                quotes.remove(i);
                break;
            }
        }
    }
    
    public void updateNow()
    {
        quoteService.restart();
    }
    
    public void exportQuotes()
    {
        PrintWriter wr = null;
        try {
            char sep = '\t';
            String exportFilePathStr = appPrefs.getAutoExportFilePath();
            wr = new PrintWriter(new FileWriter(exportFilePathStr, false));
            List<ColumnLayout> exportColumns = appPrefs.getExportColumnLayoutList();
            ObservableList<Quote> quotes = getQuoteData();
            for (Quote quote : quotes)
            {
                StringBuilder sb = new StringBuilder();
                for (ColumnLayout cl : exportColumns)
                {
                    String formattedColValue = "";
                    switch ( cl.getQuoteFieldName() )
                    {
                    case "ticker":
                        String quoteStr = quote.getTicker();
                        formattedColValue = (quoteStr != null) ? quoteStr.trim() : "";
                        break;
                        
                    case "price":
                        formattedColValue = DecFormatter.format(quote.getPrice());
                        break;
                        
                    case "change":
                        formattedColValue = DecFormatter.format(quote.getChange());
                        break;
                        
                    case "changePct":
                        formattedColValue = DecFormatter.format(quote.getChangePct()) +'%';
                        break;
                        
                    case "quoteDate":
                        LocalDate quoteDate = quote.getQuoteDate();
                        formattedColValue = (quoteDate != null) ? DateFormatter.format(quoteDate) : "";
                        break;
                        
                    case "quoteTime":
                        LocalTime quoteTime = quote.getQuoteTime();
                        formattedColValue = (quoteTime != null) ? TimeFormatter.format(quoteTime) : "";
                        break;
                        
                    case "volume":
                        sb.append(VolFormatter.format(quote.getVolume()));
                        break;
                        
                    case "high52":
                        sb.append(DecFormatter.format(quote.getHigh52()));
                        break;
                        
                    case "low52":
                        sb.append(DecFormatter.format(quote.getLow52()));
                        break;
                        
                    case "peRatio":
                        sb.append(DecFormatter.format(quote.getPeRatio()));
                        break;
                        
                    case "exDivDate":
                        LocalDate exDivDate = quote.getExDivDate();
                        formattedColValue = (exDivDate != null) ? DateFormatter.format(exDivDate) : "";
                        break;
                       
                    case "dividend":
                        sb.append(DecFormatter.format(quote.getDividend()));
                        break;
                        
                    case "fullName":
                        String fullName = quote.getFullName();
                        formattedColValue = (fullName != null) ? fullName.trim() : "";
                        break;
                        
                    case "open":
                        sb.append(DecFormatter.format(quote.getOpen()));
                        break;
                       
                    case "prevClose":
                        sb.append(DecFormatter.format(quote.getPrevClose()));
                        break;
                    }
                    
                    sb.append(formattedColValue).append(sep);
                }
               
                wr.println(sb.toString());
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if (wr != null ) {
               wr.close();
            }
        }
    }
}
