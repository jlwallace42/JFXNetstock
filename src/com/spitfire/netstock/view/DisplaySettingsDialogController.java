package com.spitfire.netstock.view;

import java.util.ArrayList;
import java.util.List;

import com.spitfire.netstock.MainApp;
import com.spitfire.netstock.model.ColumnLayout;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class DisplaySettingsDialogController
{
    @FXML
    private ListView<ColumnLayout>    availableListView;    
    @FXML
    private ListView<ColumnLayout>    selectedListView;    
    @FXML
    private Button      moveRightButton;    
    @FXML
    private Button      moveLeftButton;    
    @FXML
    private Button      moveUpButton;    
    @FXML
    private Button      moveDownButton;    
    @FXML
    private Button      okButton;    
    @FXML
    private Button      cancelButton;
    
    private Stage       dialogStage;
    
    private ObservableList<ColumnLayout>  availableList;
    private ObservableList<ColumnLayout>  selectedList;
    
    private boolean     successfulOK;
    
    
    @FXML
    private void initialize()
    {
        availableListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        availableListView.setCellFactory(list -> {
            return new ListCell<ColumnLayout>() {
                @Override
                protected void updateItem(ColumnLayout item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    }
                    else {
                        setText(item.getColumnLabel());
                    }
                }
            };
        });
        availableListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if ( newValue != null && newValue.intValue() != -1 )
            {
                moveRightButton.setDisable(false);
            }
            else
            {
                moveRightButton.setDisable(true);
            }
        });
        availableListView.focusedProperty().addListener((observable, oldValue, newValue) -> {
           if ( Boolean.TRUE.equals(newValue) ) {
               selectedListView.getSelectionModel().clearSelection();
           }
        });
        
        selectedListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        selectedListView.setCellFactory(list -> {
            return new ListCell<ColumnLayout>() {
                @Override
                protected void updateItem(ColumnLayout item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    }
                    else {
                        setText(item.getColumnLabel());
                    }
                }
            };
        });
        selectedListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if ( newValue != null && newValue.intValue() != -1 )
            {
                int selectedPos = newValue.intValue();
                int lastPos     = selectedListView.getItems().size() - 1;
                
                moveLeftButton.setDisable( selectedPos == 0 );
                moveUpButton.setDisable( selectedPos <= 1 );
                moveDownButton.setDisable( selectedPos == 0 || selectedPos == lastPos );
            }
            else
            {
                moveLeftButton.setDisable(true);
                moveUpButton.setDisable(true);
                moveDownButton.setDisable(true);
            }
        });
        selectedListView.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if ( Boolean.TRUE.equals(newValue) ) {
                availableListView.getSelectionModel().clearSelection();
            }
         });
        
        moveRightButton.setDisable(true);
        moveLeftButton.setDisable(true);
        moveUpButton.setDisable(true);
        moveDownButton.setDisable(true);
        okButton.setDisable(false);
        cancelButton.setDisable(false);
    }

    public Stage setupDialogStage(MainApp mainApp, Pane dialogPane, Stage primaryStage)
    {
        dialogStage  = new Stage();
        dialogStage.setTitle("Define list view columns");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene dialogScene = new Scene(dialogPane);
        dialogStage.setScene(dialogScene);          
        availableList = createAvailableList(ColumnLayout.getAllAvailableColumnList(), selectedList);
        availableListView.setItems(availableList);
        selectedListView.setItems(selectedList);
        
        return dialogStage;
    }
    
    private ObservableList<ColumnLayout> createAvailableList(List<ColumnLayout> fullList, List<ColumnLayout> skipList)
    {
        ObservableList<ColumnLayout> results = FXCollections.observableArrayList();
        for (ColumnLayout candidateCL : fullList)
        {
            String candidateName = candidateCL.getQuoteFieldName();
            boolean shouldSkip   = false;
            for (ColumnLayout skipCL : skipList)
            {
                if ( candidateName.equals(skipCL.getQuoteFieldName()) )
                {
                    shouldSkip = true;
                    break;
                }
            }
            
            if ( !shouldSkip )
                results.add(candidateCL);
        }
        
        return results;
    }
    
    public boolean wasSuccessfulOK()
    {
        return successfulOK;
    }
    
    public List<ColumnLayout> getSelectedList()
    {
        List<ColumnLayout> results = null;
        if ( selectedList != null )
        {
            results = new ArrayList<ColumnLayout>(selectedList);
        }
        return results;
    }
    
    public void setSelectedList(List<ColumnLayout> columnList)
    {
        this.selectedList = FXCollections.observableArrayList();
        this.selectedList.addAll(columnList);
    }
            
    @FXML
    private void onMoveRight()
    {
        int selectedPos = availableListView.getSelectionModel().getSelectedIndex();
        if ( selectedPos != -1 )
        {
            ObservableList<ColumnLayout> availableList = availableListView.getItems();
            ColumnLayout itemToMove = availableList.get(selectedPos);
            availableList.remove(selectedPos);
            selectedListView.getItems().add(itemToMove);
        }
    }
    
    @FXML
    private void onMoveLeft()
    {
        int selectedPos = selectedListView.getSelectionModel().getSelectedIndex();
        if ( selectedPos != -1 )
        {
            ObservableList<ColumnLayout> selectedList = selectedListView.getItems();
            ColumnLayout itemToMove = selectedList.get(selectedPos);
            selectedList.remove(selectedPos);
            availableListView.getItems().add(itemToMove);
        }
    }
    
    @FXML
    private void onMoveUp()
    {
        int originalPos = selectedListView.getSelectionModel().getSelectedIndex();
        if ( originalPos > 1 )
        {
            ObservableList<ColumnLayout> selectedList = selectedListView.getItems();
            ColumnLayout itemToMove = selectedList.get(originalPos);
            selectedList.remove(originalPos);
            int newPos = originalPos - 1;
            selectedList.add(newPos, itemToMove);
            selectedListView.getSelectionModel().select(newPos);
        }
    }
    
    @FXML
    private void onMoveDown()
    {
        ObservableList<ColumnLayout> selectedList = selectedListView.getItems();
        int originalPos = selectedListView.getSelectionModel().getSelectedIndex();
        if ( originalPos < (selectedList.size() - 1) )
        {
            ColumnLayout itemToMove = selectedList.get(originalPos);
            selectedList.remove(originalPos);
            int newPos = originalPos + 1;
            if ( newPos <= selectedList.size() )
                selectedList.add(newPos, itemToMove);
            else
                selectedList.add(itemToMove);
            selectedListView.getSelectionModel().select(newPos);
        }
    }
    
    @FXML
    private void onOK()
    {
        successfulOK = true;
        dialogStage.close();
    }
    
    @FXML
    private void onCancel()
    {
        successfulOK = false;
        dialogStage.close();
    }
    

}
