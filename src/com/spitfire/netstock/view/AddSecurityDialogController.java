package com.spitfire.netstock.view;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import com.spitfire.netstock.MainApp;

public class AddSecurityDialogController
{
    @FXML
    private TextField   securitySymbolTextField;
    
    @FXML
    private Button      okButton;
    
    private MainApp     mainApp;
    private Stage       dialogStage;
    
        
    public Stage setupDialogStage(MainApp mainApp, AnchorPane dialogPane, Stage primaryStage)
    {
        this.mainApp = mainApp;
        dialogStage  = new Stage();
        dialogStage.setTitle("Add Security Ticker");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene dialogScene = new Scene(dialogPane);
        dialogStage.setScene(dialogScene);  
        
        return dialogStage;
    }

    
    @FXML
    private void initialize()
    {
        securitySymbolTextField.setText("");
        
        StringProperty sp = securitySymbolTextField.textProperty();
        sp.addListener(
            (observable, oldValue, newValue) -> controlOkButton(newValue));
        
        okButton.setDisable(true);
    }
    
    private void controlOkButton(String newValue)
    {
        String newStr = (newValue != null) ? newValue.trim() : "";
        boolean okDisabled = (newStr.length() == 0);
        okButton.setDisable(okDisabled);
    }
    
    @FXML
    private void onOK()
    {
        String newValue = getSecuritySymbolText();
        if ( newValue.length() > 0 )
            mainApp.addNewSecuritySymbol(newValue.toUpperCase());
        
        dialogStage.close();
    }
    
    @FXML
    private void onCancel()
    {
        dialogStage.close();
    }
    
    
    private String getSecuritySymbolText()
    {
        String newValue = "";
        StringProperty sp = securitySymbolTextField.textProperty();
        String rawValue = sp.getValue();
        System.out.println("StringProperty.getValue() : " + rawValue);
        if ( rawValue != null)
            newValue = rawValue.trim();
        
        return newValue;
    }
}

