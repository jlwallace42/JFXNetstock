package com.spitfire.netstock.view;


import javafx.application.Platform;
import javafx.fxml.FXML;

import com.spitfire.netstock.MainApp;

public class RootLayoutController
{
    private MainApp mainApp;
    
    public void setMainApp(MainApp mainApp) 
    {
        this.mainApp = mainApp;
    }
    
    // "File" menu handlers...
    @FXML
    private void handleFile_UpdateNow()
    {
        System.out.println("Menu File | Update Now");
        mainApp.updateNow();
    }
    
    @FXML
    private void handleFile_Exit()
    {
        Platform.exit();
    }
        
    
    // "Settings" menu handlers
    @FXML
    private void handleSettings_AddSecurity()
    {
        mainApp.showAddSecurityDialog();
    }
    
    @FXML
    private void handleSettings_DisplayColumns()
    {
        mainApp.showDisplaySettingsDialog();
    }
    
    @FXML
    private void handleSettings_Export()
    {
        mainApp.showExportSettingsDialog();
    }
    
    @FXML
    private void handleSettings_Network()
    {
        System.out.println("Menu Settings | Network...");
    }
    
    
    // "Help" menu handlers
    @FXML
    private void handleHelp_About()
    {
        System.out.println("Menu Help | About...");
    }
    
}
