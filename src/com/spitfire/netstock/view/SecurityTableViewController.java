package com.spitfire.netstock.view;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.spitfire.netstock.MainApp;
import com.spitfire.netstock.model.ColumnLayout;
import com.spitfire.netstock.model.Quote;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;

public class SecurityTableViewController
{
    private static final DateTimeFormatter TimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private static final DecimalFormat     DecFormatter  = new DecimalFormat("#0.00");
    private static final DecimalFormat     VolFormatter  = new DecimalFormat("###,###,###");
    private static final Font              ArrowFont     = Font.font("Arial", 16);
    private static final String            UpArrow       = new String(new char[] { 0x25B2 });
    private static final String            DownArrow     = new String(new char[] { 0x25BC });
    
    private static List<ColumnLayout>  ColumnLayouts;
    
    private List<ColumnLayout>  columnLayouts;
    
    private List<ColumnLayout> getColumnLayouts()
    {
        if ( this.columnLayouts == null )
        {
            this.columnLayouts = SecurityTableViewController.ColumnLayouts;
        }
        
        return this.columnLayouts;
    }
    
    public static void initColumnLayouts(List<ColumnLayout> columnLayouts)
    {
        SecurityTableViewController.ColumnLayouts = columnLayouts;
    }
    
    
    // Reference to main application.
    private MainApp mainApp;
    
    @FXML
    private TableView<Quote>    quoteTable;
    
    
    @FXML
    private void initialize()
    {        
        defineColumns();
        defineRowFactory(quoteTable);
    }
    
    public void resetColumns(List<ColumnLayout> columnLayouts)
    {
        this.columnLayouts = columnLayouts;
        quoteTable.getColumns().clear();
        defineColumns();
    }
    
    public void defineColumns()
    {
        ObservableList<TableColumn<Quote,?>> tblCols = quoteTable.getColumns();
        
        // Inject a first column that is always the UP/DOWN Arrow symbol.
        {
            TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
            col.setText("");    // Header label
            col.setPrefWidth(30);
            col.setMaxWidth(30);
            col.setSortable(false);
            col.setEditable(false);
            col.setCellValueFactory(cellData -> cellData.getValue().changeProperty().asObject());
            col.setCellFactory(column -> {
                return new TableCell<Quote, Double>() {
                    @Override
                    protected void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item == null || empty) {
                            setText(null);
                        }
                        else {
                            setFont(ArrowFont);
                            if ( item.doubleValue() > 0.0 ) {
                                setText(UpArrow);
                                setTextFill(Color.GREEN);
                            }
                            else if ( item.doubleValue() < 0.0 ) {
                                setText(DownArrow);
                                setTextFill(Color.RED);
                            }
                            else {
                                setText(null);
                            }
                        }
                    }
                 };
              });
            tblCols.add(col);
        }        
        
        // Provide for per-column specific format and behavior.
        List<ColumnLayout> columnLayouts = getColumnLayouts();
        for (ColumnLayout columnLayout : columnLayouts)
        {
            switch (columnLayout.getColumnLabel())
            {
              case "Ticker" :
              {
                TableColumn<Quote, String> col = new TableColumn<Quote, String>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(true);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().tickerProperty());
                tblCols.add(col);
                break;
              }
                
              case "Price" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().priceProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                col.setCellFactory(column -> {
                    return new TableCell<Quote, Double>() {
                        @Override
                        protected void updateItem(Double item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item == null || empty) {
                                setText(null);
                            }
                            else {
                                setText(DecFormatter.format(item));
                            }
                        }
                     };
                  });
                tblCols.add(col);
                break;
              }
              
              case "Change" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().changeProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                col.setCellFactory(column -> {
                    return new TableCell<Quote, Double>() {
                        @Override
                        protected void updateItem(Double item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item == null || empty) {
                                setText(null);
                            }
                            else {
                                setText(DecFormatter.format(item));
                                if ( item.doubleValue() < 0.0 )
                                    setTextFill(Color.RED);
                                else
                                    setTextFill(Color.GREEN);
                            }
                        }
                     };
                  });
                tblCols.add(col);
                break;
              }
              
              case "Chg(%)" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().changePctProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                col.setCellFactory(column -> {
                    return new TableCell<Quote, Double>() {
                        @Override
                        protected void updateItem(Double item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item == null || empty) {
                                setText(null);
                            }
                            else {
                                setText(DecFormatter.format(item) + '%');
                                if ( item.doubleValue() < 0.0 )
                                    setTextFill(Color.RED);
                                else
                                    setTextFill(Color.GREEN);
                            }
                        }
                     };
                  });
                tblCols.add(col);
                break;
              }
              
              case "Date" :
              {
                TableColumn<Quote, LocalDate> col = new TableColumn<Quote, LocalDate>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().quoteDateProperty());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "Time" :
              {
                TableColumn<Quote, LocalTime> col = new TableColumn<Quote, LocalTime>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().quoteTimeProperty());
                col.setStyle("-fx-alignment: top-right;");
                col.setCellFactory(column -> {
                    return new TableCell<Quote, LocalTime>() {
                        @Override
                        protected void updateItem(LocalTime item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item == null || empty) {
                                setText(null);
                            }
                            else {
                                setText(TimeFormatter.format(item));
                            }
                        }
                     };
                  });
                tblCols.add(col);
                break;
              }
              
              case "Volume" :
              {
                TableColumn<Quote, Integer> col = new TableColumn<Quote, Integer>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().volumeProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                col.setCellFactory(column -> {
                    return new TableCell<Quote, Integer>() {
                        @Override
                        protected void updateItem(Integer item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item == null || empty) {
                                setText(null);
                            }
                            else {
                                setText(VolFormatter.format(item));
                            }
                        }
                     };
                  });
                tblCols.add(col);
                break;
              }
              
              case "52 High" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().high52Property().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "52 Low" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().low52Property().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "P/E Ratio" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().peRatioProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "ExDiv Date" :
              {
                TableColumn<Quote, LocalDate> col = new TableColumn<Quote, LocalDate>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().exDivDateProperty());
                tblCols.add(col);
                break;
              }
              
              case "Dividend" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().dividendProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "Full Name" :
              {
                TableColumn<Quote, String> col = new TableColumn<Quote, String>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().fullNameProperty());
                tblCols.add(col);
                break;
              }
              
              case "Open" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().openProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
              
              case "Prev Close" :
              {
                TableColumn<Quote, Double> col = new TableColumn<Quote, Double>();
                col.setText(columnLayout.getColumnLabel());
                col.setPrefWidth(columnLayout.getPreferredWidth().intValue());
                col.setSortable(false);
                col.setEditable(false);
                col.setCellValueFactory(cellData -> cellData.getValue().prevCloseProperty().asObject());
                col.setStyle("-fx-alignment: top-right;");
                tblCols.add(col);
                break;
              }
            }
        }
    }
    
    private void defineRowFactory(TableView<Quote> tblView)
    {
        tblView.setRowFactory(new Callback<TableView<Quote>, TableRow<Quote>>() {
            @Override
            public TableRow<Quote> call(TableView<Quote> tableView) {
                final TableRow<Quote> row = new TableRow<Quote>();
                final ContextMenu rowMenu = new ContextMenu();
                MenuItem deleteMenuItem = new MenuItem("Delete");
                deleteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        Quote quote = row.getItem();
                        String ticker = quote.getTicker();
                        if ( mainApp.showDeleteSecurityDialog(ticker) )
                        {
                            tblView.getItems().remove(quote);
                        }
                    }
                });
                rowMenu.getItems().add(deleteMenuItem);
          
                // Only display context menu for non-null items...
                row.contextMenuProperty().bind(
                    Bindings.when(Bindings.isNotNull(row.itemProperty()))
                    .then(rowMenu)
                    .otherwise((ContextMenu)null));
                    
                return row;
            }
          });
    }
    
    
    public void setMainApp(MainApp mainApp)
    {
        this.mainApp = mainApp;
        
        quoteTable.setItems(this.mainApp.getQuoteData());
    }
}
