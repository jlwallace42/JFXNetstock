package com.spitfire.netstock.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a single quote instance for a specific security.
 */
public class Quote implements Comparable<Quote>
{
    // Maps possible column labels to the corresponding field name of this class.
    public static final Map<String, String> LabelToFieldMap;
    static
    {
        LabelToFieldMap = new LinkedHashMap<String, String>();
        LabelToFieldMap.put("Ticker",       "ticker");
        LabelToFieldMap.put("Price",        "price");
        LabelToFieldMap.put("Change",       "change");
        LabelToFieldMap.put("Chg(%)",       "changePct");
        LabelToFieldMap.put("Date",         "quoteDate");
        LabelToFieldMap.put("Time",         "quoteTime");
        LabelToFieldMap.put("Volume",       "volume");
        LabelToFieldMap.put("52 High",      "high52");
        LabelToFieldMap.put("52 Low",       "low52");
        LabelToFieldMap.put("P/E Ratio",    "peRatio");
        LabelToFieldMap.put("ExDiv Date",   "exDivDate");
        LabelToFieldMap.put("Dividend",     "dividend");
        LabelToFieldMap.put("Full Name",    "fullName");
        LabelToFieldMap.put("Open",         "open");
        LabelToFieldMap.put("Prev Close",   "prevClose");
    };
    
    
    private final StringProperty            ticker;
    private final DoubleProperty            price;
    private final DoubleProperty            change;
    private final DoubleProperty            changePct;
    private final ObjectProperty<LocalDate> quoteDate;
    private final ObjectProperty<LocalTime> quoteTime;
    private final IntegerProperty           volume;
    private final DoubleProperty            high52;
    private final DoubleProperty            low52;
    private final DoubleProperty            peRatio;
    private final ObjectProperty<LocalDate> exDivDate;
    private final DoubleProperty            dividend;
    private final StringProperty            fullName;
    private final DoubleProperty            open;
    private final DoubleProperty            prevClose;
    
    
    public Quote(String ticker) {
        LocalDate nowDate = LocalDate.now();
        LocalTime nowTime = LocalTime.now();
        this.ticker     = new SimpleStringProperty(ticker);
        this.price      = new SimpleDoubleProperty(42.42);
        this.change     = new SimpleDoubleProperty(-0.67);
        this.changePct  = new SimpleDoubleProperty(-0.0003);
        this.quoteDate  = new SimpleObjectProperty<LocalDate>(nowDate);
        this.quoteTime  = new SimpleObjectProperty<LocalTime>(nowTime);
        this.volume     = new SimpleIntegerProperty(932456);
        this.high52     = new SimpleDoubleProperty(46.32);
        this.low52      = new SimpleDoubleProperty(3.45);
        this.peRatio    = new SimpleDoubleProperty(-1);
        this.exDivDate  = new SimpleObjectProperty<LocalDate>(nowDate);
        this.dividend   = new SimpleDoubleProperty(-1);
        this.fullName   = new SimpleStringProperty(ticker);
        this.open       = new SimpleDoubleProperty(-1);
        this.prevClose  = new SimpleDoubleProperty(-1);
    }


    public final String getTicker()
    {
        return ticker.get();
    }

    public final void setTicker(String value)
    {
        this.ticker.set(value);
    }
    
    public final StringProperty tickerProperty()
    {
        return ticker;
    }


    public final double getPrice()
    {
        return price.get();
    }

    public final void setPrice(double value)
    {
        this.price.set(value);
    }
    
    public final DoubleProperty priceProperty()
    {
        return price;
    }


    public final double getChange()
    {
        return change.get();
    }

    public final void setChange(double value)
    {
        this.change.set(value);
    }
    
    public final DoubleProperty changeProperty()
    {
        return change;
    }


    public final double getChangePct()
    {
        return changePct.get();
    }

    public final void setChangePct(double value)
    {
        this.changePct.set(value);
    }
    
    public final DoubleProperty changePctProperty()
    {
        return changePct;
    }


    public final LocalDate getQuoteDate()
    {
        return quoteDate.get();
    }

    public final void setQuoteDate(LocalDate value)
    {
        this.quoteDate.set(value);
    }
    
    public final ObjectProperty<LocalDate> quoteDateProperty()
    {
        return quoteDate;
    }


    public final LocalTime getQuoteTime()
    {
        return quoteTime.get();
    }


    public final void setQuoteTime(LocalTime value)
    {
        this.quoteTime.set(value);
    }
    
    public final ObjectProperty<LocalTime> quoteTimeProperty()
    {
        return quoteTime;
    }


    public final int getVolume()
    {
        return volume.get();
    }

    public final void setVolume(int value)
    {
        this.volume.set(value);
    }
    
    public final IntegerProperty volumeProperty()
    {
        return volume;
    }


    public final double getHigh52()
    {
        return high52.get();
    }

    public final void setHigh52(double value)
    {
        this.high52.set(value);
    }
    
    public DoubleProperty high52Property()
    {
        return high52;
    }


    public final double getLow52()
    {
        return low52.get();
    }

    public final void setLow52(double value)
    {
        this.low52.set(value);
    }
    
    public final DoubleProperty low52Property()
    {
        return low52;
    }


    public final double getPeRatio()
    {
        return peRatio.get();
    }

    public final void setPeRatio(double value)
    {
        this.peRatio.set(value);
    }
    
    public final DoubleProperty peRatioProperty()
    {
        return peRatio;
    }


    public final LocalDate getExDivDate()
    {
        return exDivDate.get();
    }

    public final void setExDivDate(LocalDate value)
    {
        this.exDivDate.set(value);
    }
    
    public final ObjectProperty<LocalDate> exDivDateProperty()
    {
        return exDivDate;
    }


    public final double getDividend()
    {
        return dividend.get();
    }

    public final void setDividend(double value)
    {
        this.dividend.set(value);
    }
    
    public final DoubleProperty dividendProperty()
    {
        return dividend;
    }


    public final String getFullName()
    {
        return fullName.get();
    }

    public final void setFullName(String value)
    {
        this.fullName.set(value);
    }
    
    public final StringProperty fullNameProperty()
    {
        return fullName;
    }


    public final double getOpen()
    {
        return open.get();
    }

    public final void setOpen(double value)
    {
        this.open.set(value);
    }
    
    public final DoubleProperty openProperty()
    {
        return open;
    }


    public final double getPrevClose()
    {
        return prevClose.get();
    }

    public final void setPrevClose(double value)
    {
        this.prevClose.set(value);
    }
    
    public final DoubleProperty prevCloseProperty()
    {
        return prevClose;
    }
    
    
    public String toJson()
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
    
    public static Quote fromJSON(String jsonStr)
    {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, Quote.class);
    }


    @Override
    public int compareTo(Quote that)
    {
        if ( that == null ) {
            throw new NullPointerException();
        }
        
        return this.ticker.get().compareTo(that.ticker.get());
    }
    
}
