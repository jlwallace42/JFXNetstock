package com.spitfire.netstock.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * Represents layout information concerning a single column.
 */
public class ColumnLayout
{
    private static final List<ColumnLayout> AllAvailableColumnsList;
    static
    {
        List<ColumnLayout> tmpList = new ArrayList<ColumnLayout>();
        tmpList.add( new ColumnLayout("Ticker",     "ticker",       "") );
        tmpList.add( new ColumnLayout("Price",      "price",        "-#0.00") );
        tmpList.add( new ColumnLayout("Change",     "change",       "-#0.00") );
        tmpList.add( new ColumnLayout("Chg(%)",     "changePct",    "-#0.00") );
        tmpList.add( new ColumnLayout("Date",       "quoteDate",    "MM/dd/yyyy") );
        tmpList.add( new ColumnLayout("Time",       "quoteTime",    "HH:mm") );
        tmpList.add( new ColumnLayout("Volume",     "volume",       "") );
        tmpList.add( new ColumnLayout("52 High",    "high52",       "-#0.00") );
        tmpList.add( new ColumnLayout("52 low",     "low52",        "-#0.00") );
        tmpList.add( new ColumnLayout("P/E Ratio",  "peRatio",      "-#0.00") );
        tmpList.add( new ColumnLayout("ExDiv Date", "exDivDate",    "") );
        tmpList.add( new ColumnLayout("Dividend",   "dividend",     "-#0.00") );
        tmpList.add( new ColumnLayout("Full Name",  "fullName",     "") );
        tmpList.add( new ColumnLayout("Open",       "open",         "-#0.00") );
        tmpList.add( new ColumnLayout("Prev Close", "prevClose",    "-#0.00") );
        
        AllAvailableColumnsList = Collections.unmodifiableList(tmpList);
    };
    
    private static final List<ColumnLayout> DefaultColumnsList;
    static
    {
        List<ColumnLayout> tmpList = new ArrayList<ColumnLayout>();
        tmpList = new ArrayList<ColumnLayout>();
        tmpList.add( new ColumnLayout("Ticker", "ticker",    "") );
        tmpList.add( new ColumnLayout("Price",  "price",     "#.##") );
        tmpList.add( new ColumnLayout("Change", "change",    "-#.##") );
        tmpList.add( new ColumnLayout("Chg(%)", "changePct", "-#.##") );
        tmpList.add( new ColumnLayout("Date",   "quoteDate", "") );
        tmpList.add( new ColumnLayout("Time",   "quoteTime", "") );
        tmpList.add( new ColumnLayout("Volume", "volume",    "") );
        
        DefaultColumnsList = Collections.unmodifiableList(tmpList);
    };
    
    
    private String  columnLabel;
    private String  quoteFieldName;
    private String  formatPattern;
    private Integer preferredWidth;
    
    public ColumnLayout()
    {
        this.preferredWidth = 100;
        this.formatPattern  = "";
    }
    
    public ColumnLayout(String columnLabel, String quoteFieldName, String formatPattern)
    {
        this.columnLabel    = columnLabel;
        this.quoteFieldName = quoteFieldName;
        this.formatPattern  = formatPattern;
        this.preferredWidth = 100;
    }
    
    @XmlElement(name = "columnLabel")
    public String getColumnLabel()
    {
        return columnLabel;
    }
    
    public void setColumnLabel(String columnLabel)
    {
        this.columnLabel = columnLabel;
    }
    
    @XmlElement(name = "quoteFieldName")
    public String getQuoteFieldName()
    {
        return quoteFieldName;
    }
    
    public void setQuoteFieldName(String quoteFieldName)
    {
        this.quoteFieldName = quoteFieldName;
    }
    
    @XmlElement(name = "formatPattern", nillable=true)
    public String getFormatPattern()
    {
        return formatPattern;
    }
    
    public void setFormatPattern(String formatPattern)
    {
        this.formatPattern = formatPattern;
    }
    
    @XmlElement(name = "preferredWidth")
    public Integer getPreferredWidth()
    {
        return preferredWidth;
    }
    
    public void setPreferredWidth(Integer preferredWidth)
    {
        this.preferredWidth = preferredWidth;
    }
    
    public static List<ColumnLayout> getAllAvailableColumnList()
    {
        return new ArrayList<ColumnLayout>(AllAvailableColumnsList);
    }
    
    public static List<ColumnLayout> getDefaultColumnLayoutList()
    {
        return new ArrayList<ColumnLayout>(DefaultColumnsList);
    }
}